module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  testMatch: [
    "**/__tests__/*.{j,t}s?(x)",
    "**/tests/unit/**/*_spec.{j,t}s?(x)"
  ],
  transformIgnorePatterns: [
    "node_modules/(?!(@gitlab/(ui|svgs)"
    + "|bootstrap-vue"
    + ")/)",
  ],
};
