/* eslint-disable import/prefer-default-export */
import GlListIncident from './list_incident.vue';
import GlHead from './header.vue';
import GlMarkdown from './markdown.vue';
import GlComment from './comment.vue';

export { GlListIncident, GlHead, GlMarkdown, GlComment };
