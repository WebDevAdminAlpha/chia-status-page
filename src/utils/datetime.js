/* eslint-disable import/prefer-default-export */
export const dateTimeMixin = {
  methods: {
    formatDate(dateStr) {
      return (
        new Date(dateStr).toLocaleString('en-US', {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          timeZone: 'UTC',
        }) || ''
      );
    },
    formatDateTime(dateStr) {
      return (
        new Date(dateStr).toLocaleString('en-US', {
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          hour: 'numeric',
          hour12: true,
          minute: 'numeric',
        }) || ''
      );
    },
    formatDateTimeZone(dateStr) {
      return new Date(dateStr).toString().match(/([A-Z]+[+-][0-9]+)/)[1] || '';
    },
  },
};
